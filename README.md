# Insight

Insight is a tool which allows you to gain "insight" into certain information across various torrent clients. Please note this project is in its very early stages, **so there is a lot of bugs (to fix!)**.

## Features
* View table of best performing torrents across both Deluge/qBittorrent
* View table of under performing torrents across both Deluge/qBittorrent
* Panels which show various statistics regarding ratios/data transferred
    - Ratio panel which shows mean/median/stdev of all torrent ratios across 
    - Tracker panel which shows your stats across various trackers
* Extremely customizable ```trackers.json``` 

## Install Guide
1. Clone this repo: ```git clone https://gitlab.com/gQmXcWEp/insight2.git``` and ```cd Insight2```
2. Open up ```config.toml.example```, replace applicable values
3. Rename config.toml.example to config.toml via ```mv config.toml.example config.toml```
4. Install requirements via ```pip3 install -r requirements.txt```   
4. Run ```python3 main.py``` in a console of your choice
5. Customize other settings (such as table column order), if you want to
5. Read **Deployment Notes** for more information 

## Deployment Notes 
As **HTTPS support for qBittorrent or Deluge is not implemented**, it is strongly advised you deploy this on the same server that your torrent clients are running on. Then, when you would like to run Insight, you SSH into the server, and run it from there.



## Planned
- [x] Support for Deluge
- [x] Customize order of columns in config.toml
- [x] Basic tracker label support   
- [ ] FIX BUGS

### Immediate
- [x] Enhanced torrent tracker support (file with hosts -> labels)
- [x] Graceful exiting
- [x] Add enhanced config settings for organizing panels on main screen
- [ ] FIX BUGS

### Very Long Term
- [ ] Support for rTorrent
- [ ] FIX BUGS


