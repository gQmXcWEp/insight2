import statistics

from rich.panel import Panel
from lib.ratio_engine import RatioEngine


# noinspection PyShadowingNames
class GeneralStatsPanel:
    def __init__(self, ratio_engine: RatioEngine):
        """
        GeneralStatsPanel stores panels rendered below tables, these include
        - Ratio Panel (mean/median/stdev of ratios)
        - Success Rate Panel (% ratio above 1.00)
        - Data Panel (amount of data in, amount of data out, net gain)

        :param ratio_engine: Ratio Engine instance to gather data from
        """
        self._ratio_engine = ratio_engine
        self._mean_ratio = self._ratio_engine.calculate_mean_ratio()

        all_torrent_ratios = [torrent.get_ratio() for torrent in self._ratio_engine.get_torrent_info()]

        # If there is not at least 2 torrents in the client, median/stdev can't be calculated
        self._median_ratio = statistics.median(all_torrent_ratios) if len(all_torrent_ratios) > 2 else 0
        self._stdev_ratio = statistics.stdev(all_torrent_ratios) if len(all_torrent_ratios) > 2 else 0

        self._net_pos_neg = self._ratio_engine.net_positive_ratios()

        if self._net_pos_neg[0] + self._net_pos_neg[1] > 0:
            self._percent_net_pos = self._net_pos_neg[0] / (self._net_pos_neg[0] + self._net_pos_neg[1])
        else:
            self._percent_net_pos = 0

        for i in range(len(self._net_pos_neg)):
            if self._net_pos_neg[i] is None:
                self._net_pos_neg[i] = 0

    def get_ratio_panel(self):
        """
        Ratio Panel displays the mean, median, and standard deviation for a given client (regardless of tag, or tracker)

        :return: Renderable panel
        """
        ratio_panel = Panel("[cyan]Mean Ratio: [bold]{:.2f}[/bold][/cyan]\n"
                            "[yellow]Median Ratio: [bold]{:.2f}[/bold][/yellow]\n"
                            "[white]Standard Deviation: [bold]{:.2f}[/bold][/white]"
                            .format(self._mean_ratio,
                                    self._median_ratio,
                                    self._stdev_ratio),
                            title="Ratio")

        return ratio_panel

    def get_success_rate_panel(self):
        """
        Success rate panel displays number of torrents above a 1.00 ratio, below a 1.00 ratio, and the success rate
        overall (# ratio > 1.00 / total)

        :return: Renderable panel
        """
        success_rate_panel = Panel("[green]Ratio above 1.00: [bold]{:.2f}[/bold][/green]\n"
                                   "[yellow]Ratio below 1.00: [bold]{:.2f}[/bold][/yellow]\n"
                                   "[white]Success Rate:[/white] [green][bold]{:.2f}%[/bold][/green]"
                                   .format(self._net_pos_neg[0],
                                           self._net_pos_neg[1],
                                           self._percent_net_pos * 100),
                                   title="Ratio Success Rate")

        return success_rate_panel

    def get_data_panel(self):
        data_panel = Panel("[green]Total Uploaded: [bold]{}[/bold][/green]\n"
                           "[yellow]Total Downloaded: [bold]{}[/bold][/yellow]\n"
                           "[white]Net Gain:[/white] [green][bold]{}[/bold][/green]"
                           .format(self._ratio_engine.net_total_data()[0],
                                   self._ratio_engine.net_total_data()[1],
                                   self._ratio_engine.net_total_data()[2]),
                           title="Data")

        return data_panel





