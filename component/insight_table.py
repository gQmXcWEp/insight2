
class InsightTable:
    def __init__(self, column_order):
        """
        InsightTable is a class of functions shared by all tables in Insight

        :param column_order: Column order of table
        """
        self._column_order = column_order

    def _construct_string(self, **kwargs):
        """

        :param kwargs: Dictionary of items to translate
        :return: Row string to be displayed on table
        """

        string_dict = {
            "name": f"{kwargs['name']}||",
            "size": f"{kwargs['size']}||",
            "uploaded": f"{kwargs['uploaded']}||",
            "ratio": f"{kwargs['ratio']}||",
            "active": f"{kwargs['active']}||",
            "tracker": f"{kwargs['tracker']}||"
        }

        result_string = " "
        for i in self._column_order:
            result_string += string_dict[i]

        return result_string[0:len(result_string) - 2]

    @staticmethod
    def _translate_ratio(ratio: float, threshold: float) -> str:
        """
        Translate raw ratio into string with color corresponding to ratio performance (relative to threshold).
        Green -> 80%+ of threshold
        Yellow -> 50 to 80% of threshold
        Red -> Less than 50% of threshold

        :param ratio: Ratio to color code
        :param threshold: Threshold to measure
        :return: Color coded ratio
        """

        raw_torrent_ratio = float("{:.2f}".format(ratio))
        if raw_torrent_ratio >= (0.8 * threshold):
            torrent_ratio = "[green]{}".format(raw_torrent_ratio)
        elif raw_torrent_ratio >= (0.5 * threshold):
            torrent_ratio = "[yellow]{}".format(raw_torrent_ratio)
        else:
            torrent_ratio = "[red]{}".format(raw_torrent_ratio)

        return torrent_ratio

    @staticmethod
    def _sorted_data(raw_data: list, sort_key: str, reverse: bool) -> list:
        """
        Take raw torrent data and sort it by given sort_key

        Supported sort keys:
        - uploaded
        - total_size
        - time_active
        - ratio
        - name
        - added_on

        :param raw_data: Raw torrent data
        :param sort_key: Key to sort by
        :param reverse: Whether or not to reverse
        :return:
        """

        def _translate_sort_key(torrent_obj, attribute):
            """

            :param torrent_obj: Torrent Object
            :param attribute: Attribute to get
            :return: Attribute of torrent_obj
            """

            sort_key_dict = {
                "uploaded": torrent_obj.get_uploaded(),
                "total_size": torrent_obj.get_total_size(),
                "time_active": torrent_obj.get_time_active(),
                "ratio": torrent_obj.get_ratio(),
                "name": torrent_obj.get_name(),
                "added_on": torrent_obj.get_added_on()
            }

            return sort_key_dict[attribute]

        return sorted(raw_data, key=lambda x: _translate_sort_key(x, sort_key), reverse=reverse)


