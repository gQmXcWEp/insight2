from hurry.filesize import size
from rich.panel import Panel

from lib.ratio_engine import RatioEngine


# noinspection PyShadowingNames
class NetworkStatsPanel:
    def __init__(self, ratio_engine: RatioEngine):
        self.ratio_engine = ratio_engine

    def get_panel(self):
        net_stats = self.ratio_engine.net_stat_data()

        network_panel = Panel("[green]Upload Speed: [bold]{}/s[/bold][/green]\n"
                              "[cyan]Download Speed: [bold]{}/s[/bold][/cyan]\n".format(str(size(net_stats[0])),
                                                                                        str(size(net_stats[1]))),
                              title="Network")
        return network_panel
