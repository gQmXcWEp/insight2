import datetime

from hurry.filesize import size
from rich import box
from rich.table import Table
from rich.panel import Panel

from lib.ratio_engine import RatioEngine
from lib.tracker_engine import TrackerEngine

from component.insight_table import InsightTable

from config.config import Config


class RatioBragTable(InsightTable):
    def __init__(self, ratio_engine: RatioEngine, tracker_engine: TrackerEngine, config: Config, table_title: str):
        """
        Object which represents ratio brag table

        :param ratio_engine: Ratio engine instance to pull data from
        :param tracker_engine: Tracker engine to translate tracker URLs
        :param config: Config instance to get config values from
        :param table_title: Title of table
        """
        self._ratio_engine = ratio_engine
        self._tracker_engine = tracker_engine

        self._number_rows = config.get_ratio_brag_rows()
        self._column_order = config.get_ratio_brag_column_order()

        self._table_title = table_title

        self._sort_key = config.get_ratio_brag_sort_key()

        super().__init__(self._column_order)

    def get_table(self):
        """

        :return: Renderable rich table
        """
        ratio_brag_table = Table(show_header=True, header_style='bold #2070b2',
                                 title=self._table_title)
        ratio_brag_table.box = box.MINIMAL
        ratio_brag_table.border_style = "white"

        for i in self._column_order:
            if i == "name":
                ratio_brag_table.add_column("[cyan bold]Name", justify="right")
            elif i == "size":
                ratio_brag_table.add_column("[magenta bold]Size", justify="right")
            elif i == "uploaded":
                ratio_brag_table.add_column("[white bold]Uploaded", justify="left")
            elif i == "ratio":
                ratio_brag_table.add_column("[green bold]Ratio", justify="right")
            elif i == "active":
                ratio_brag_table.add_column("[gray bold]Active", justify="left")
            elif i == "tracker":
                ratio_brag_table.add_column("[yellow bold]Tracker", justify="right")

        best_torrents = self._sorted_data(self._ratio_engine.get_torrent_info(),
                                          self._sort_key,
                                          True)[slice(0, self._number_rows)]

        for torrent in best_torrents:

            # Torrent name formatting
            ratio_brag_torrent_name = torrent.get_name()
            if len(ratio_brag_torrent_name) > 45:
                ratio_brag_torrent_name = ratio_brag_torrent_name[:42] + "..."
            else:
                ratio_brag_torrent_name = ratio_brag_torrent_name[:45]

            ratio_brag_torrent_name = "[cyan]{}".format(ratio_brag_torrent_name) if len(ratio_brag_torrent_name) > 0 \
                else "ERROR"

            # Torrent size formatting
            ratio_brag_torrent_size = "[magenta] {}".format(size(torrent.get_size())) if torrent.get_size() >= 0 else 0

            # Amount uploaded
            ratio_brag_torrent_uploaded = size(torrent.get_uploaded()) if torrent.get_uploaded() >= 0 else 0

            # Torrent ratio formatting
            ratio_brag_torrent_ratio = self._translate_ratio(torrent.get_ratio(), 1.00) if \
                isinstance(torrent.get_ratio(), float) else 0

            ratio_brag_torrent_active = str(datetime.timedelta(seconds=torrent.get_time_active())) if \
                isinstance(torrent.get_time_active(), int) else 0

            # Torrent tracker
            ratio_brag_torrent_tracker = self._tracker_engine.translate_tracker(torrent.get_tracker())

            row_string = self._construct_string(
                **{"name": ratio_brag_torrent_name,
                   "size": ratio_brag_torrent_size,
                   "uploaded": ratio_brag_torrent_uploaded,
                   "ratio": ratio_brag_torrent_ratio,
                   "active": ratio_brag_torrent_active,
                   "tracker": ratio_brag_torrent_tracker}
            )

            # Error handling if row string is invalid
            if len(row_string) <= 0:
                return Panel("[yellow] Something has gone wrong.", title="[red] Error")

            if "||" not in row_string:
                return Panel("[yellow] You must have more than one column.", title="[red] Error")

            ratio_brag_table.add_row(*row_string.split("||"))

        return ratio_brag_table
