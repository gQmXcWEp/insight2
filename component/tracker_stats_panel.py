import statistics

from rich.panel import Panel

from lib.ratio_engine import RatioEngine
from lib.tracker_engine import TrackerEngine
from hurry.filesize import size


class TrackerStatsPanel:
    def __init__(self, ratio_engine: RatioEngine, tracker_engine: TrackerEngine):
        """
        TrackerStatsPanel stores panels which report per-tracker statistics
        - Mean ratio for tracker
        - Median ratio for tracker
        - Stdev of ratios for tracker

        - Total uploaded for tracker
        - Total downloaded for tracker
        - Net gain for tracker

        :param ratio_engine: Ratio engine instance to use to pull data from
        :param tracker_engine: Tracker engine instance to translate trackers from
        """
        self._ratio_engine = ratio_engine
        self._tracker_engine = tracker_engine

    def get_trackers_panel(self) -> list:
        ratio_data = self._ratio_engine.calculate_tracker_ratio_data()
        raw_up_data, raw_down_data = self._ratio_engine.calculate_tracker_data_transfer()

        panels = []
        if len(ratio_data.items()) > 0:
            for tracker, ratio_list in ratio_data.items():
                if len(ratio_list) >= 2:
                    mean_ratio = sum(ratio_list) / len(ratio_list) if len(ratio_list) > 0 else 0
                    median_ratio = statistics.median(ratio_list) if len(ratio_list) > 2 else 0
                    stdev_ratio = statistics.stdev(ratio_list) if len(ratio_list) > 2 else 0

                    total_uploaded = sum(raw_up_data[tracker])
                    total_downloaded = sum(raw_down_data[tracker])
                    raw_net_gain = total_uploaded - total_downloaded

                    net_gain = size(raw_net_gain) if raw_net_gain > 0 else -1 * size(abs(raw_net_gain))

                    if tracker != "":
                        panels.append(Panel("[cyan]Mean Ratio: [bold]{:.2f}[/bold][/cyan]\n"
                                            "[yellow]Median Ratio: [bold]{:.2f}[/bold][/yellow]\n"
                                            "[white]Standard Deviation: [bold]{:.2f}[/bold][/white]\n"
                                            "\n"
                                            "[green]Total Uploaded: [bold]{}[/bold][/green]\n"
                                            "[yellow]Total Downloaded: [bold]{}[/bold][/yellow]\n"
                                            "[white]Net Gain:[/white] [green][bold]{} [/bold][/green][white]({:.2f}%)"
                                            .format(mean_ratio,
                                                    median_ratio,
                                                    stdev_ratio,
                                                    size(total_uploaded),
                                                    size(total_downloaded),
                                                    net_gain,
                                                    float(
                                                        total_uploaded / total_downloaded) * 100)
                                            if total_downloaded > 0 else 0,
                                            title=f"{self._tracker_engine.translate_tracker(tracker)} [white]Stats"))
        else:
            panels.append(Panel("[yellow] No trackers found.",  title="[white][bold] Tracker Stats"))

        return panels
