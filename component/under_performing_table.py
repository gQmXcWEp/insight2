import datetime

from hurry.filesize import size
from rich import box
from rich.table import Table
from component.insight_table import InsightTable

from config.config import Config
from lib import ratio_engine


# noinspection PyShadowingNames
class UnderPerformingTable(InsightTable):
    def __init__(self, ratio_engine: ratio_engine, config: Config, table_title: str):
        """
        Object which represents under performing table

        :param ratio_engine: Ratio engine object instance to gather data from
        :param config: Config object instance to gather row data from
        :param table_title: Title of table
        """

        self.ratio_engine = ratio_engine

        self._number_rows = config.get_under_performing_rows()
        self._column_order = config.get_under_performing_column_order()
        self._sort_key = config.get_under_performing_sort_key()

        self._table_title = table_title

        super().__init__(self._column_order)

    def get_table(self):
        """

        :return: Renderable rich table
        """
        table = Table(show_header=True, header_style='bold #2070b2',
                      title=self._table_title)
        table.box = box.MINIMAL
        table.border_style = "white"

        for i in self._column_order:
            if i == "name":
                table.add_column("[cyan bold]Name", justify="right")
            elif i == "size":
                table.add_column("[magenta bold]Size", justify="right")
            elif i == "uploaded":
                table.add_column("[white bold]Uploaded", justify="left")
            elif i == "ratio":
                table.add_column("[green bold]Ratio", justify="right")
            elif i == "active":
                table.add_column("[gray bold]Active", justify="left")

        threshold = 0.65
        below_065_unsorted = self.ratio_engine.get_below_threshold(threshold)
        below_065 = self._sorted_data(below_065_unsorted,
                                      self._sort_key,
                                      False)[slice(0, self._number_rows)]

        for torrent in below_065:

            # Torrent name formatting
            torrent_name = torrent.get_name()
            if len(torrent_name) > 45:
                torrent_name = torrent_name[:42] + "..."
            else:
                torrent_name = torrent_name[:45]

            torrent_name = "[cyan]{}".format(torrent_name)

            # Torrent size formatting
            torrent_size = "[magenta] {}".format(size(torrent.get_size()))

            # Amount uploaded
            torrent_uploaded = size(torrent.get_uploaded())

            # Torrent ratio formatting
            torrent_ratio = self._translate_ratio(torrent.get_ratio(), threshold)

            torrent_active = str(datetime.timedelta(seconds=torrent.get_time_active()))

            row_string = self._construct_string(
                **{"name": torrent_name,
                   "size": torrent_size,
                   "uploaded": torrent_uploaded,
                   "ratio": torrent_ratio,
                   "active": torrent_active}
            )

            table.add_row(*row_string.split("||"))

        return table
