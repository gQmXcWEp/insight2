import toml


class Config:
    def __init__(self, path):
        """
        Config stores all config.toml values in memory for quick access

        :param path: Path to config.toml
        """
        with open(path, 'r') as file:
            file_string = file.read()

            self._config_data = toml.loads(file_string)

    def get_qbittorrent_address(self) -> str:
        """

        :return: Address of qbittorrent (e.g my.server.xyz)
        """
        return self._config_data["qbittorrent-credentials"]["address"]

    def get_qbittorrent_port(self) -> str:
        """

        :return: Port of qbittorrent (e.g 12345)
        """
        return self._config_data["qbittorrent-credentials"]["port"]

    def get_qbittorrent_username(self) -> str:
        """

        :return: Username for qbittorrent (e.g cool_username)
        """
        return self._config_data["qbittorrent-credentials"]["username"]

    def get_qbittorrent_password(self) -> str:
        """

        :return: Password for qbittorrent (e.g secret_password)
        """
        return self._config_data["qbittorrent-credentials"]["password"]

    def get_deluge_address(self) -> str:
        """

        :return: Address of deluge (e.g my.server.xyz)
        """
        return self._config_data["deluge-credentials"]["address"]

    def get_deluge_port(self) -> str:
        """

        :return: Port of deluge (e.g 12345)
        """
        return self._config_data["deluge-credentials"]["port"]

    def get_deluge_username(self) -> str:
        """

        :return: Username for deluge (e.g cool_username)
        """
        return self._config_data["deluge-credentials"]["username"]

    def get_deluge_password(self) -> str:
        """

        :return: Password for deluge (e.g secret_password)
        """
        return self._config_data["deluge-credentials"]["password"]

    def get_ratio_brag_rows(self) -> int:
        """

        :return: Number of rows to display in ratio brag table
        """
        return self._config_data["ratio-brag-table"]["number-rows"]

    def get_ratio_brag_column_order(self) -> list:
        """

        :return: Order of columns in ratio brag table
        """
        return self._config_data["ratio-brag-table"]["column-order"]

    def get_under_performing_rows(self) -> int:
        """

        :return: Number of rows to display in under performing table
        """
        return self._config_data["under-performing-table"]["number-rows"]

    def get_under_performing_column_order(self) -> list:
        """

        :return: Order of columns in under performing table
        """
        return self._config_data["under-performing-table"]["column-order"]

    def get_update_interval(self) -> int:
        """

        :return: Number of seconds between torrent client queries
        """
        return self._config_data["global-settings"]["update-interval"]

    def get_ratio_brag_sort_key(self) -> str:
        """

        :return: Sort key of ratio brag table
        """

        return self._config_data["ratio-brag-table"]["sort-key"]

    def get_under_performing_sort_key(self) -> str:
        """

        :return: Sort key of under performing table
        """

        return self._config_data["under-performing-table"]["sort-key"]

    def get_client_mode(self) -> str:
        """

        :return: Which client(s) to display
        """

        return self._config_data["global-settings"]["client-mode"]
