import time

from rich.columns import Columns
from rich.console import Console
from rich.console import RenderGroup
from rich.live import Live
from rich.panel import Panel
from rich.style import Style

from component.global_stats_panel import GeneralStatsPanel
from component.network_stats_panel import NetworkStatsPanel
from component.tracker_stats_panel import TrackerStatsPanel

from component.ratio_brag_table import RatioBragTable

from config.config import Config

from lib.ratio_engine import RatioEngine
from lib.tracker_engine import TrackerEngine


def main_loop(config: Config, console: Console):

    # Setup qbit client
    qbit_address = config.get_qbittorrent_address()
    qbit_port = config.get_qbittorrent_port()
    qbit_username = config.get_qbittorrent_username()
    qbit_password = config.get_qbittorrent_password()

    # Setup deluge client
    deluge_address = config.get_deluge_address()
    deluge_username = config.get_deluge_username()
    deluge_password = config.get_deluge_password()
    deluge_port = config.get_deluge_port()

    # Setup tracker engine
    tracker_engine = TrackerEngine('trackers.json')

    # Setup ratio engine
    ratio_engine_qbit = RatioEngine(qbit_address,
                                    qbit_port,
                                    qbit_username,
                                    qbit_password,
                                    tracker_engine)
    ratio_engine_deluge = RatioEngine(deluge_address,
                                      int(deluge_port),
                                      deluge_username,
                                      deluge_password,
                                      tracker_engine,
                                      mode="DELUGE")

    ratio_engine_deluge.update_torrent_info()
    ratio_engine_qbit.update_torrent_info()

    # Setup all panels
    general_stats_panel_qbit = GeneralStatsPanel(ratio_engine_qbit)
    network_stats_panel_qbit = NetworkStatsPanel(ratio_engine_qbit)
    tracker_stats_panel_qbit = TrackerStatsPanel(ratio_engine_qbit, tracker_engine)

    general_stats_panel_deluge = GeneralStatsPanel(ratio_engine_deluge)
    network_stats_panel_deluge = NetworkStatsPanel(ratio_engine_deluge)
    tracker_stats_panel_deluge = TrackerStatsPanel(ratio_engine_deluge, tracker_engine)

    # Setup ratio brag table
    ratio_brag_table_qbit = RatioBragTable(ratio_engine_qbit, tracker_engine, config,
                                           "[white][bold]QBit[/bold][/white] [white] best performers")
    ratio_brag_table_deluge = RatioBragTable(ratio_engine_deluge, tracker_engine, config,
                                             "[blue][bold]Deluge[/bold][/blue] [white] best performers")

    # Main loop
    with Live(console=console, transient=True) as live:
        while True:
            ratio_engine_qbit.update_torrent_info()

            live.update(
                RenderGroup(
                    Panel("[yellow][bold]Insight"),

                    Columns([
                        # TABLES GO HERE
                        Columns(
                            [ratio_brag_table_qbit.get_table(),
                             ratio_brag_table_deluge.get_table()], align="center", expand=True
                        ),

                        # GLOBAL STATS GO HERE
                        Panel(
                            Columns(
                                [network_stats_panel_qbit.get_panel(),
                                 general_stats_panel_qbit.get_ratio_panel(),
                                 general_stats_panel_qbit.get_data_panel()], align="center", expand=True
                            ),
                            title="[white][bold]QBit Global Stats[/bold][/white]",
                            style=Style(color="white", bold=True)
                        ),
                        Panel(
                            Columns(
                                [network_stats_panel_deluge.get_panel(),
                                 general_stats_panel_deluge.get_ratio_panel(),
                                 general_stats_panel_deluge.get_data_panel()], align="center", expand=True
                            ),
                            title="[bold][blue]Deluge[/blue] [white]Global Stats[/white][/bold]",
                            style=Style(color="blue", bold=True)
                        ),
                        # New line to separate panels
                        "\n",

                        # TRACKER STATS PANEL GOES HERE
                        Panel(
                            Columns(
                                tracker_stats_panel_qbit.get_trackers_panel(), align="center", expand=True
                            ),
                            title="[white][bold]QBit Tracker Stats[/bold][/white]",
                            style=Style(color="white", bold=True)
                        ),

                        # New line to reduce clutter
                        "\n",

                        Panel(
                            Columns(
                                tracker_stats_panel_deluge.get_trackers_panel(), align="center", expand=True
                            ),
                            title="[bold][blue]Deluge[/blue] [white]Tracker Stats[/white][/bold]",
                            style=Style(color="blue", bold=True)
                        )

                    ]),
                ))

            time.sleep(config.get_update_interval())
