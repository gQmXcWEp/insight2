import time

from rich.columns import Columns
from rich.console import Console
from rich.console import RenderGroup
from rich.live import Live
from rich.panel import Panel

from component.global_stats_panel import GeneralStatsPanel
from component.network_stats_panel import NetworkStatsPanel
from component.tracker_stats_panel import TrackerStatsPanel

from component.ratio_brag_table import RatioBragTable

from config.config import Config

from lib.ratio_engine import RatioEngine
from lib.tracker_engine import TrackerEngine


def main_loop(config: Config, console: Console):

    # Setup qbit client
    qbit_address = config.get_qbittorrent_address()
    qbit_port = config.get_qbittorrent_port()
    qbit_username = config.get_qbittorrent_username()
    qbit_password = config.get_qbittorrent_password()

    # Setup tracker engine
    tracker_engine_qbit = TrackerEngine('trackers.json')

    # Setup ratio engine
    ratio_engine_qbit = RatioEngine(qbit_address,
                                    qbit_port,
                                    qbit_username,
                                    qbit_password,
                                    tracker_engine_qbit)
    ratio_engine_qbit.update_torrent_info()

    # Setup all panels
    general_stats_panel_qbit = GeneralStatsPanel(ratio_engine_qbit)
    network_stats_panel_qbit = NetworkStatsPanel(ratio_engine_qbit)
    tracker_stats_panel_qbit = TrackerStatsPanel(ratio_engine_qbit, tracker_engine_qbit)

    # Setup ratio brag table
    ratio_brag_table_qbit = RatioBragTable(ratio_engine_qbit, tracker_engine_qbit, config,
                                           "[white][bold]QBit[/bold][/white] [white] best performers")

    # Main loop
    with Live(console=console, transient=True) as live:
        while True:
            ratio_engine_qbit.update_torrent_info()

            live.update(
                RenderGroup(
                    Panel("[yellow][bold]Insight"),

                    Columns([
                        # TABLES GO HERE
                        Columns(
                            [ratio_brag_table_qbit.get_table()], align="center", expand=True
                        ),

                        # GLOBAL STATS GO HERE
                        Panel(
                            Columns(
                                [network_stats_panel_qbit.get_panel(),
                                 general_stats_panel_qbit.get_ratio_panel(),
                                 general_stats_panel_qbit.get_data_panel()], align="center", expand=True
                            ),
                            title="[white][bold]Global Stats[/bold][/white]"
                        ),
                        # New line to separate panels
                        "\n",

                        # TRACKER STATS PANEL GOES HERE
                        Columns(
                            tracker_stats_panel_qbit.get_trackers_panel(), align="center", expand=True
                        )
                    ]),
                ))

            time.sleep(config.get_update_interval())
