from hurry.filesize import size

from lib.torrent.delugeclient import DelugeClient
from lib.torrent.qbitclient import QBitClient

from lib.tracker_engine import TrackerEngine


class RatioEngine:
    def __init__(self, host: str, port: str, username: str, password: str, tracker_engine: TrackerEngine, mode="QBIT"):
        """
        RatioEngine takes large amount of raw data gathered from torrent clients and calculates data based on it
        (mean, median, standard deviation of ratios, data transferred etc). It can be seen as the "brain" behind
        Insight's CLI

        :param host: Host of torrent client
        :param port: Port of torrent client
        :param username: Username of torrent client
        :param password: Password of torrent client
        :param tracker_engine: Tracker engine instance to use
        :param mode: Client to use, QBIT or DELUGE supported (default is QBIT)
        """

        if mode == "QBIT":
            host = f"{host}:{port}"
            self.client = QBitClient(host, username, password)

        elif mode == "DELUGE":
            self.client = DelugeClient(host, port, username, password)

        self.tracker_engine = tracker_engine
        self.client.get_torrent_statistics()

    def calculate_mean_ratio(self) -> float:
        """
        Calculate the mean ratio across all torrents in a single client

        :return: Mean of all ratios in client
        """
        total_ratio = 0
        number_torrents = 0

        for torrent in self.get_torrent_info():
            total_ratio += torrent.get_ratio()
            number_torrents += 1

        return (total_ratio / number_torrents) if number_torrents > 0 else 0

    def net_positive_ratios(self):
        """
        Return the number of ratios above 1.00 and below 1.00

        :return: Number of ratios above 1.00, number of ratios below 1.00
        """
        net_positive_ratio = 0
        net_negative_ratio = 0

        for torrent in self.get_torrent_info():
            if torrent.get_ratio() >= 1:
                net_positive_ratio += 1
            else:
                net_negative_ratio += 1

        return net_positive_ratio, net_negative_ratio

    def get_below_threshold(self, threshold: float) -> list:
        """
        Return a list of all torrents that don't meet the specified threshold ratio

        :param threshold: Cut off threshold (anything below this specified value is returned)
        :return: List of torrents with ratio below specified threshold
        """
        torrents_below_threshold = []

        for torrent in self.get_torrent_info():
            if torrent.get_ratio() <= threshold:
                torrents_below_threshold.append(torrent)

        return torrents_below_threshold

    def net_total_data(self):
        """
        Calculate the total data uploaded and downloaded across all active torrents in given client. Also calculate
        net positive upload (total uploaded - total downloaded)

        :return: Total data uploaded, total data downloaded, net positive uploaded
        """
        total_downloaded = 0
        total_uploaded = 0

        for torrent in self.get_torrent_info():
            total_downloaded += torrent.get_size()
            total_uploaded += torrent.get_uploaded()

        net_diff = total_uploaded - total_downloaded

        return size(total_uploaded), size(total_downloaded), size(net_diff) if net_diff >= 0 else -1 * size(
            abs(net_diff))

    def net_stat_data(self):
        """

        :return: Active data outgoing, active data incoming
        """
        total_up_speed = 0
        total_dl_speed = 0

        for torrent in self.get_torrent_info():
            total_up_speed += torrent.get_upspeed()
            total_dl_speed += torrent.get_dlspeed()

        return total_up_speed, total_dl_speed

    def calculate_tracker_ratio_data(self):
        """
        Gather all ratio data, then insert into dictionary based on tracker:
        {tracker_name: [list_of_ratios]}

        """

        tracker_ratios = {}
        for torrent in self.get_torrent_info():
            torrent_tracker = self.tracker_engine.translate_tracker(torrent.get_tracker())

            if torrent_tracker in tracker_ratios:
                tracker_ratios[torrent_tracker].append(torrent.get_ratio())
            else:
                tracker_ratios[torrent_tracker] = [torrent.get_ratio()]

        return tracker_ratios

    def calculate_tracker_data_transfer(self):
        """
        Calculate the total down/up for each given tracker

        :return: Dictionary of tracker up stats {tracker: torrents}, and tracker down stats {tracker: torrents}
        """

        tracker_up_data_stats = {}
        tracker_down_data_stats = {}

        for torrent in self.get_torrent_info():
            torrent_tracker = self.tracker_engine.translate_tracker(torrent.get_tracker())

            if torrent_tracker in tracker_up_data_stats:
                tracker_up_data_stats[torrent_tracker].append(torrent.get_uploaded())
            else:
                tracker_up_data_stats[torrent_tracker] = [torrent.get_uploaded()]

            if torrent_tracker in tracker_down_data_stats:
                tracker_down_data_stats[torrent_tracker].append(torrent.get_downloaded())
            else:
                tracker_down_data_stats[torrent_tracker] = [torrent.get_downloaded()]

        return tracker_up_data_stats, tracker_down_data_stats

    def get_torrent_info(self):
        """

        :return: Raw torrent information
        """
        return self.client.get_torrent_info()

    def update_torrent_info(self):
        """
        Tells torrent client to re-gather all torrent information

        """
        self.client.get_torrent_statistics()
