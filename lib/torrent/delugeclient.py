from deluge_client import DelugeRPCClient

from lib.torrent.delugetorrent import DelugeTorrent

# These are all values that can be gotten from DelugeRPCClient (which is what we want)
_STATUS_KEYS = [
    'state',
    'download_location',
    'tracker_host',
    'tracker_status',
    'next_announce',
    'name',
    'total_size',
    'progress',
    'num_seeds',
    'total_seeds',
    'num_peers',
    'total_peers',
    'eta',
    'download_payload_rate',
    'upload_payload_rate',
    'ratio',
    'distributed_copies',
    'num_pieces',
    'piece_length',
    'total_done',
    'files',
    'file_priorities',
    'file_progress',
    'peers',
    'is_seed',
    'is_finished',
    'active_time',
    'seeding_time',
    'time_since_transfer',
    'last_seen_complete',
    'seed_rank',
    'all_time_download',
    'total_uploaded',
    'total_payload_download',
    'total_payload_upload',
    'time_added',
    'label'
]


class DelugeClient:
    def __init__(self, address: str, port: int, username: str, password: str):
        """
        DelugeClient is an abstraction layer that operates in front of DelugeRPCClient to gather all torrent data into
        a list which can be queried by RatioEngine to calculate various statistics

        :param address:
        :param port:
        :param username:
        :param password:
        """
        self.client = DelugeRPCClient(
            address,
            port,
            username,
            password)

        self.torrent_info = []

    def get_torrent_statistics(self):
        self.torrent_info.clear()

        response = self.client.call('core.get_torrents_status', {}, _STATUS_KEYS)

        for torrent in response:
            state_value = response[torrent][b'state']
            download_location_value = response[torrent][b'download_location'].decode("utf-8")
            tracker_host_value = response[torrent][b'tracker_host'].decode("utf-8")
            tracker_status_value = response[torrent][b'tracker_status'].decode("utf-8")
            next_announce_value = response[torrent][b'next_announce']
            name_value = response[torrent][b'name'].decode("utf-8")
            total_size_value = response[torrent][b'total_size']
            progress_value = response[torrent][b'progress']
            num_seeds_value = response[torrent][b'num_seeds']
            total_seeds_value = response[torrent][b'total_seeds']
            num_peers_value = response[torrent][b'num_peers']
            total_peers_value = response[torrent][b'total_peers']
            eta_value = response[torrent][b'eta']
            download_payload_rate_value = response[torrent][b'download_payload_rate']
            upload_payload_rate_value = response[torrent][b'upload_payload_rate']
            ratio_value = response[torrent][b'ratio']
            distributed_copies_value = response[torrent][b'distributed_copies']
            num_pieces_value = response[torrent][b'num_pieces']
            piece_length_value = response[torrent][b'piece_length']
            total_done_value = response[torrent][b'total_done']
            files_value = response[torrent][b'files']
            file_priorities_value = response[torrent][b'file_priorities']
            file_progress_value = response[torrent][b'file_progress']
            peers_value = response[torrent][b'peers']
            is_seed_value = response[torrent][b'is_seed']
            is_finished_value = response[torrent][b'is_finished']
            active_time_value = response[torrent][b'active_time']
            seeding_time_value = response[torrent][b'seeding_time']
            time_since_transfer_value = response[torrent][b'time_since_transfer']
            last_seen_complete_value = response[torrent][b'last_seen_complete']
            seed_rank_value = response[torrent][b'seed_rank']
            all_time_download_value = response[torrent][b'all_time_download']
            total_uploaded_value = response[torrent][b'total_uploaded']
            total_payload_download_value = response[torrent][b'total_payload_download']
            total_payload_upload_value = response[torrent][b'total_payload_upload']
            time_added_value = response[torrent][b'time_added']
            label_value = response[torrent][b'label'].decode("utf-8")

            self.torrent_info.append(
                DelugeTorrent(state_value, download_location_value, tracker_host_value, tracker_status_value,
                              next_announce_value, name_value, total_size_value, progress_value,
                              num_seeds_value, total_seeds_value, num_peers_value, total_peers_value,
                              eta_value, download_payload_rate_value, upload_payload_rate_value, ratio_value,
                              distributed_copies_value, num_pieces_value, piece_length_value, total_done_value,
                              files_value, file_priorities_value, file_progress_value, peers_value,
                              is_seed_value, is_finished_value, active_time_value, seeding_time_value,
                              time_since_transfer_value, last_seen_complete_value, seed_rank_value,
                              all_time_download_value, total_uploaded_value, total_payload_download_value,
                              total_payload_upload_value, time_added_value, label_value))

    def get_torrent_info(self):
        return self.torrent_info
