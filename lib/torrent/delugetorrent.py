class DelugeTorrent:
    def __init__(self, state, download_location, tracker_host, tracker_status, next_announce, name, total_size,
                 progress, num_seeds, total_seeds, num_peers, total_peers, eta,
                 download_payload_rate, upload_payload_rate, ratio, distributed_copies,
                 num_pieces, piece_length, total_done, files, file_priorities, file_progress, peers, is_seed,
                 is_finished, active_time, seeding_time, time_since_transfer, last_seen_complete, seed_rank,
                 all_time_download, total_uploaded, total_payload_download, total_payload_upload, time_added, label):
        self.state = state
        self.download_location = download_location
        self.tracker_host = tracker_host
        self.tracker_status = tracker_status
        self.next_announce = next_announce
        self.name = name
        self.total_size = total_size
        self.progress = progress
        self.num_seeds = num_seeds
        self.total_seeds = total_seeds
        self.num_peers = num_peers
        self.total_peers = total_peers
        self.eta = eta
        self.download_payload_rate = download_payload_rate
        self.upload_payload_rate = upload_payload_rate
        self.ratio = ratio
        self.distributed_copies = distributed_copies
        self.num_pieces = num_pieces
        self.piece_length = piece_length
        self.total_done = total_done
        self.files = files
        self.file_priorities = file_priorities
        self.file_progress = file_progress
        self.peers = peers
        self.is_seed = is_seed
        self.is_finished = is_finished
        self.active_time = active_time
        self.seeding_time = seeding_time
        self.time_since_transfer = time_since_transfer
        self.last_seen_complete = last_seen_complete
        self.seed_rank = seed_rank
        self.all_time_download = all_time_download
        self.total_uploaded = total_uploaded
        self.total_payload_download = total_payload_download
        self.total_payload_upload = total_payload_upload
        self.time_added = time_added
        self.label = label

    def get_state(self):
        return self.state

    def get_download_location(self):
        return self.download_location

    # Modified to match qbit code
    def get_tracker(self):
        return self.tracker_host

    def get_tracker_status(self):
        return self.tracker_status

    def get_next_announce(self):
        return self.next_announce

    def get_name(self):
        return self.name

    # Modified to match qbit code
    def get_size(self):
        return self.total_size

    # Added to match qbit code
    def get_total_size(self):
        return self.total_size

    def get_progress(self):
        return self.progress

    def get_num_seeds(self):
        return self.num_seeds

    def get_total_seeds(self):
        return self.total_seeds

    def get_num_peers(self):
        return self.num_peers

    def get_total_peers(self):
        return self.total_peers

    def get_eta(self):
        return self.eta

    # Modified to match qbit
    def get_dlspeed(self):
        return self.download_payload_rate

    # Modified to match qbit
    def get_upspeed(self):
        return self.upload_payload_rate

    def get_ratio(self):
        return self.ratio

    def get_distributed_copies(self):
        return self.distributed_copies

    def get_num_pieces(self):
        return self.num_pieces

    def get_piece_length(self):
        return self.piece_length

    def get_total_done(self):
        return self.total_done

    def get_files(self):
        return self.files

    def get_file_priorities(self):
        return self.file_priorities

    def get_file_progress(self):
        return self.file_progress

    def get_peers(self):
        return self.peers

    def get_is_seed(self):
        return self.is_seed

    def get_is_finished(self):
        return self.is_finished

    # Modified to match qbit
    def get_time_active(self):
        return self.active_time

    def get_seeding_time(self):
        return self.seeding_time

    def get_time_since_transfer(self):
        return self.time_since_transfer

    def get_last_seen_complete(self):
        return self.last_seen_complete

    def get_seed_rank(self):
        return self.seed_rank

    # Modified to match qbit
    def get_downloaded(self):
        return self.all_time_download

    # Modified to match qbit
    def get_uploaded(self):
        return self.total_uploaded

    def get_total_payload_download(self):
        return self.total_payload_download

    def get_total_payload_upload(self):
        return self.total_payload_upload

    # Modified to match qbit
    def get_added_on(self):
        return self.time_added

    def get_label(self):
        return self.label
