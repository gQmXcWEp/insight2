import qbittorrentapi as qbit

from lib.torrent.qbittorrent import QBitTorrent


class QBitClient:
    def __init__(self, address, username, password):
        """
        QBitClient is an abstraction layer which operates in front of qbittorrentapi to query all torrent data from
        qBittorrent

        :param address: Address of qbit client in format 'server:port'
        :param username: Username of qbit client
        :param password: Password of qbit client
        """

        # Login to qbit client
        self.client = qbit.Client(address)
        self.client.auth_log_in(username, password)

        self.torrent_info = []

    def get_torrent_statistics(self):
        """
        Query qbit for all torrent data, then insert that data into a list which can be queried by ratio engine to
        calculate various statistics

        """

        # Clear torrents_info before updating, as to not have duplicate data
        self.torrent_info.clear()

        # Iterate through each torrent received, store it in torrent_info
        for torrent in self.client.torrents_info():
            self.torrent_info.append(QBitTorrent(*list(torrent.values())))

    def get_torrent_info(self) -> list:
        """

        :return: Raw torrent info
        """
        return self.torrent_info
