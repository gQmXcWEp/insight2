class QBitTorrent:
    def __init__(self, added_on, amount_left, auto_tmm, availability, category,
                 completed, completion_on, content_path, dl_limit, dlspeed,
                 downloaded, downloaded_session, eta, f_l_piece_prio,
                 force_start, torrent_hash, last_activity, magnet_uri, max_ratio,
                 max_seeding_time, name, num_complete, num_incomplete, num_leechs, num_seeds, priority, progress,
                 ratio, ratio_limit, save_path, seeding_time_limit, seen_complete, seq_dl,
                 size, state, super_seeding, tags, time_active, total_size, tracker,
                 trackers_count, up_limit, uploaded, uploaded_session, upspeed):
        self.uploaded_session = uploaded_session
        self.uploaded = uploaded
        self.up_limit = up_limit
        self.tracker = tracker
        self.total_size = total_size
        self.time_active = time_active
        self.tags = tags
        self.super_seeding = super_seeding
        self.state = state
        self.size = size
        self.seq_dl = seq_dl
        self.seen_complete = seen_complete
        self.seeding_time_limit = seeding_time_limit
        self.save_path = save_path
        self.ratio_limit = ratio_limit
        self.ratio = ratio
        self.progress = progress
        self.priority = priority
        self.num_seeds = num_seeds
        self.num_incomplete = num_incomplete
        self.num_leechs = num_leechs
        self.num_complete = num_complete
        self.trackers_count = trackers_count
        self.name = name
        self.max_seeding_time = max_seeding_time
        self.max_ratio = max_ratio
        self.magnet_uri = magnet_uri
        self.last_activity = last_activity
        self.hash = torrent_hash
        self.force_start = force_start
        self.eta = eta
        self.downloaded_session = downloaded_session
        self.downloaded = downloaded
        self.dlspeed = dlspeed
        self.dl_limit = dl_limit
        self.content_path = content_path
        self.completion_on = completion_on
        self.completed = completed
        self.category = category
        self.availability = availability
        self.f_l_piece_prio = f_l_piece_prio
        self.upspeed = upspeed
        self.auto_tmm = auto_tmm
        self.added_on = added_on
        self.amount_left = amount_left

    def get_added_on(self):
        return self.added_on

    def get_amount_left(self):
        return self.amount_left

    def get_auto_tmm(self):
        return self.auto_tmm

    def get_availability(self):
        return self.availability

    def get_category(self):
        return self.category

    def get_completed(self):
        return self.completed

    def get_completion_on(self):
        return self.completion_on

    def get_content_path(self):
        return self.content_path

    def get_dl_limit(self):
        return self.dl_limit

    def get_dlspeed(self):
        return self.dlspeed

    def get_downloaded(self):
        return self.downloaded

    def get_downloaded_session(self):
        return self.downloaded_session

    def get_eta(self):
        return self.eta

    def get_f_l_piece_prio(self):
        return self.f_l_piece_prio

    def get_force_start(self):
        return self.force_start

    def get_hash(self):
        return self.hash

    def get_last_activity(self):
        return self.last_activity

    def get_magnet_uri(self):
        return self.magnet_uri

    def get_max_ratio(self):
        return self.max_ratio

    def get_max_seeding_time(self):
        return self.max_seeding_time

    def get_name(self):
        return self.name

    def get_num_complete(self):
        return self.num_complete

    def get_num_incomplete(self):
        return self.num_incomplete

    def get_num_leechs(self):
        return self.num_leechs

    def get_num_seeds(self):
        return self.num_seeds

    def get_priority(self):
        return self.priority

    def get_progress(self):
        return self.progress

    def get_ratio(self):
        return self.ratio

    def get_ratio_limit(self):
        return self.ratio_limit

    def get_save_path(self):
        return self.save_path

    def get_seeding_time_limit(self):
        return self.seeding_time_limit

    def get_seen_complete(self):
        return self.seen_complete

    def get_seq_dl(self):
        return self.seq_dl

    def get_size(self):
        return self.size

    def get_state(self):
        return self.state

    def get_super_seeding(self):
        return self.super_seeding

    def get_tags(self):
        return self.tags

    def get_time_active(self):
        return self.time_active

    def get_total_size(self):
        return self.total_size

    def get_tracker(self):
        return self.tracker

    def get_trackers_count(self):
        return self.trackers_count

    def get_up_limit(self):
        return self.up_limit

    def get_uploaded(self):
        return self.uploaded

    def get_uploaded_session(self):
        return self.uploaded_session

    def get_upspeed(self):
        return self.upspeed
