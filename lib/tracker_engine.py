import json


class TrackerEngine:
    def __init__(self, tracker_file):
        with open(tracker_file) as trackers_file:
            self._tracker_data = json.load(trackers_file)

        self._translation_dict = {}

        for key, value in self._tracker_data.items():
            for tracker in value['trackers']:
                self._translation_dict[tracker] = '[{0}]{1}'.format(str(value['color']).lower(), key)

    def translate_tracker(self, url):
        """
        Translate tracker URL to neatened abbreviation

        :param url: URL to translate
        :return: Tracker abbreviation
        """
        # Neaten URL
        url = url.replace("https://", "").replace("http://", "")

        # Get root of url (drop passkey, etc.)
        if len(url.split("/")) > 1:
            url = url.split("/")[0]

        # If the url root is in translation dictionary, return it, else just return url
        if url in self._translation_dict:
            return self._translation_dict[url]
        else:
            return url
