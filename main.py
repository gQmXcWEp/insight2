import os
import sys
from config.config import Config

from rich.console import Console


def main(config: Config):
    import layout.deluge
    import layout.qbit
    import layout.all

    main_console = Console()

    options = {
        "QBIT": layout.qbit.main_loop,
        "DELUGE": layout.deluge.main_loop,
        "ALL": layout.all.main_loop
    }

    try:
        if config.get_client_mode() in options:
            options[config.get_client_mode()](config, main_console)
        else:
            main_console.print("[red] Unknown 'client-mode' value found in config.toml, aborting!")

    except KeyboardInterrupt:
        main_console.print("[white] Goodbye!")
        sys.exit(0)


if __name__ == '__main__':
    configuration = Config(os.path.join(os.getcwd(), "config.toml"))

    main(configuration)
